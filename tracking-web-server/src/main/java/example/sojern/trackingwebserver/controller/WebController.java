package example.sojern.trackingwebserver.controller;

import org.springframework.http.HttpStatus;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@RestController
public class WebController {

    @GetMapping(value = "/ping")
    public String ping() {
        File tmp = new File("tmp/ok");
        if(tmp.exists()){
            return "hello";
        } else {
            return String.valueOf(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    @GetMapping(value = "/addTmp")
    public void addTmp() {
        File tmp = new File("tmp/ok");
        try {
            File parent = tmp.getParentFile();
            if(!parent.mkdirs()){
                System.out.println("error creating directories");
            }
            tmp.createNewFile();
        } catch (IOException e) {
            System.out.println("error creating file");
        }
    }

    @GetMapping(value = "/deleteTmp")
    public void delTmp() {
        File tmp = new File("tmp/ok");
        tmp.delete();
    }

    @GetMapping(value = "/img", produces = {
            "image/gif"
    })
    public byte[] getImage() {
        System.out.println("Received request to fetch gif image");
        InputStream in = getClass().getClassLoader().getResourceAsStream("gif-image.gif");
        try {
            byte[] result = StreamUtils.copyToByteArray(in);
            return result;
        } catch (IOException e) {
            System.out.println("Error reading file");
            return null;
        }
    }

}
