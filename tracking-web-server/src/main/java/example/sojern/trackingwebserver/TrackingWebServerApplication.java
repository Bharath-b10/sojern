package example.sojern.trackingwebserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrackingWebServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrackingWebServerApplication.class, args);
	}

}
