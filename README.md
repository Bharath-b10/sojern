# React application

- [Favorite dogs](#favorite-dogs)


# Favorite dogs 

Favorite dogs is an react react app which shows image of 6 random dogs, which can be refreshed to fetch 6 new images.

Also clicking the image will add the dog image to favorites list which can be accesses using the favorite endpoint.

### Setting up the project

### Requirements

1) Latest version of Node.js and npm

### Install the dependencies

`npm install or npm ci`

### Run the server

`npm start`

The app should start and page similar to picture below should be visible

![img.png](images/img1.png)

Clicking the refresh button will give 6 new dog images

Click on the any of the image to add it to the favorites

Access the favorites by navigating to link

`http://localhost:3000/favorites`

It should give link similar to result below

![img.png](images/img2.png)

To check the app in mobile view, switch to mobile view using chrome to find result similar to shown below

![img.png](images/img6.png)


