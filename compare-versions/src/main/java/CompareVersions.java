public class CompareVersions {
    public static void main(String[] args) {

        String version1 = "1.2";
        String version2 = "1.1";
        if(args.length == 2){
            version1 = args[0];
            version2 = args[1];
        } else {
            System.out.println("Invalid arguments");
        }

        int answer = compareVersions(version1, version2);
        System.out.println("----Comparing---"+version1+" and "+version2+" = "+answer);
    }

    public static int compareVersions(String version1, String version2){
        String[] ver1 = version1.split("\\.");
        String[] ver2 = version2.split("\\.");

        int ver1Len = ver1.length;
        int ver2Len = ver2.length;

        int i = 0;
        while (i < ver1Len && i < ver2Len){
            int num1 = Integer.parseInt(ver1[i]);
            int num2 = Integer.parseInt(ver2[i]);

            if(num1 < num2)return -1;
            if(num1 > num2)return 1;
            i++;
        }

        if(ver1Len > i)return 1;
        if(ver2Len > i)return -1;
        return 0;
    }
}
