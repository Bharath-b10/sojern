import org.junit.Assert;
import org.junit.Test;

public class CompareVersionsTest {
    @Test
    public void test1() {
        String str1 = "0.1";
        String str2 = "1.1";
        int answer = CompareVersions.compareVersions(str1, str2);
        Assert.assertEquals(-1, answer);
    }

    @Test
    public void test2() {
        String str2 = "0.1";
        String str1 = "1.1";
        int answer = CompareVersions.compareVersions(str1, str2);
        Assert.assertEquals(1, answer);
    }

    @Test
    public void test3() {
        String str1 = "1.2";
        String str2 = "1.2.9.9";
        int answer = CompareVersions.compareVersions(str1, str2);
        Assert.assertEquals(-1, answer);
    }

    @Test
    public void test4() {
        String str1 = "1.4";
        String str2 = "1.4";
        int answer = CompareVersions.compareVersions(str1, str2);
        Assert.assertEquals(0, answer);
    }

    @Test
    public void test5() {
        String str1 = "1.10";
        String str2 = "1.1";
        int answer = CompareVersions.compareVersions(str1, str2);
        Assert.assertEquals(1, answer);
    }
}
