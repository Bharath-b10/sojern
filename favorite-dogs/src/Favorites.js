import React from "react"

class Favorites extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            dogImage: []
        }
    }

    componentDidMount(){
        let fav = JSON.parse(localStorage.getItem("favorites"));
        this.setState({dogImage: fav});
    }

    render() {
        return (
            <div > Your Favorite images
                {
                    <div class="flex-container">
                        {
                            this.state.dogImage.map((dogImageUrl) => {
                                return (
                                    <div>
                                        <img src={dogImageUrl} width={400} height={200} alt="" 
                                             class="dog-image" onClick={() => this.saveToFavorites(dogImageUrl)} />
                                    </div>
                                    
                                );
                            })
                        }
                    </div>
                }
            </div>

        );
    }
}

export default Favorites;