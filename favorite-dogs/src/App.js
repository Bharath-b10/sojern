import React from "react"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';
import DogImages from './DogImages.js';
import Favorites from './Favorites.js';

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<DogImages />}/>
        <Route path="/favorites" element={<Favorites />}/>
      </Routes>
    </BrowserRouter>
    //<DogImages/>
  );
}

export default App;
