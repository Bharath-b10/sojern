import React from "react"

class dogImages extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            dogImage: []
        }
    }

    getDogsImage = () => {
        fetch("https://random.dog/woof.json")
            .then(response => {
                return response.json()
            })
            .then(data => {
                let len = this.state.dogImage.length
                if(data.url.endsWith("jpg") ){
                    len = [...this.state.dogImage, data.url].length
                    this.setState({dogImage:[...this.state.dogImage, data.url]});
                }
                if(len < 6){
                    this.getDogsImage();
                }
            })
    }

    fetchNewDogImages() {
        this.setState({dogImage: []});
        this.getDogsImage();
    }

    saveToFavorites(dogImageUrl) {
        let fav = JSON.parse(localStorage.getItem("favorites"));
        if(fav === null || fav === undefined){
            fav = []
        }
        fav = [...fav, dogImageUrl]
        localStorage.setItem("favorites", JSON.stringify(fav));
        alert('Saved to favorite');
    }

    componentDidMount(){
        this.fetchNewDogImages();
    }

    render() {
        return (
            <div >
                {
                    <div class="flex-container">
                        {
                            this.state.dogImage.map((dogImageUrl) => {
                                return (
                                    <div>
                                        <img src={dogImageUrl} width={400} height={200} alt="" 
                                             class="dog-image" onClick={() => this.saveToFavorites(dogImageUrl)} />
                                    </div>
                
                                );
                            })
                        }
                    </div>
                }

                {
                    <div>
                        <button class="button" onClick={ () => this.fetchNewDogImages()}>Refresh</button>
                    </div>
                    
                }
            </div>
        );
    }
}

export default dogImages;